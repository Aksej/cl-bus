;;;; package.lisp

(defpackage #:cl-bus
  (:use #:cl)
  (:export #:bus #:bus<- #:buffer<- #:bus-read #:bus-advance #:bus-consume
           #:with-bus #:with-file-bus))
