;;;; cl-bus.asd

(asdf:defsystem #:cl-bus
  :description "A(n almost) referentially transparent interface for streams"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "cl-bus")))
