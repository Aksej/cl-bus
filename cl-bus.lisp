;;;; cl-bus.lisp

(in-package #:cl-bus)

(defstruct bus
  (buffer ""
   :type string)
  (stream nil
   :type (or stream null))
  (child nil
   :type (or bus null)))

(defgeneric bus<- (obj)
  (:documentation "Takes a string, bus, or stream and returns a bus that reads
  across that string or stream"))

(defmethod bus<- ((obj string))
  (make-bus :buffer obj))

(defmethod bus<- ((obj stream))
  (make-bus :stream obj))

(defmethod bus<- ((obj bus))
  obj)

(defun buffer<- (bus)
  "Returns the buffer of the bus."
  (declare (type bus bus))
  (concatenate 'string
               (bus-buffer bus)
               (let ((child (bus-child bus)))
                 (if child
                     (buffer<- child)
                     ""))))

(defun bus-read (bus
                 &optional
                   (length 1))
  "Returns two values. The second value is NIL if reading failed and T
  otherwise. The first value is a string of length LENGTH when reading
  succeeded, or a shorter string if reading failed. Reading fails when EOF is
  reached while reading from the underlying stream of BUS or when BUS does not
  have an underlying stream."
  (declare (type bus bus)
           (type (integer (0))
                 length))
  (let ((buffer-length (length (bus-buffer bus))))
    (if (<= length buffer-length)
        (values (subseq (bus-buffer bus)
                        0 length)
                t)
        (let ((child (bus-child bus))
              (rest-length (- length buffer-length)))
          (cond
            (child
             (multiple-value-bind (rest-buffer enough?)
                 (bus-read child rest-length)
               (values (concatenate 'string
                                    (bus-buffer bus)
                                    rest-buffer)
                       enough?)))
            ((bus-stream bus)
             (let* ((read (make-string rest-length))
                    (count (read-sequence read (bus-stream bus))))
               (setf (bus-buffer bus)
                     (concatenate 'string
                                  (bus-buffer bus)
                                  (subseq read 0 count)))
               (if (= rest-length count)
                   (values (bus-buffer bus)
                           t)
                   (progn
                     (setf (bus-stream bus)
                           nil)
                     (values (bus-buffer bus)
                             nil)))))
            (t (values (bus-buffer bus)
                       nil)))))))

(defun bus-advance (bus
                    &optional
                      (length 1))
  "Returns two values. The second value is NIL if advancing failed and T
  otherwise. The first value is a bus that starts reading LENGTH characters into
  BUS if advancing succeeded or an empty bus if advancing failed.
  Advancing fails when LENGTH is greater than the length of the buffer of BUS."
  (declare (type bus bus)
           (type (integer (0))
                 length))
  (let ((buffer-length (length (bus-buffer bus))))
    (cond
      ((< length buffer-length)
       (let ((new-bus (make-bus :buffer (subseq (bus-buffer bus)
                                                length)
                                :stream (bus-stream bus)
                                :child (bus-child bus))))
         (setf (bus-buffer bus)
               (subseq (bus-buffer bus)
                       0 length)
               (bus-child bus)
               new-bus
               (bus-stream bus)
               nil)
         (values new-bus t)))
      ((bus-child bus)
       (if (= length buffer-length)
           (values (bus-child bus)
                   t)
           (bus-advance (bus-child bus)
                        (- length buffer-length))))
      ((= length buffer-length)
       (let ((new-bus (make-bus :buffer ""
                                :stream (bus-stream bus)
                                :child nil)))
         (setf (bus-child bus)
               new-bus
               (bus-stream bus)
               nil)
         (values new-bus t)))
      (t
       (values (make-bus)
               nil)))))

(defun bus-consume (bus
                    &optional
                      (length 1))
  "Returns three values: First a string as in BUS-READ, second a bus as in
  BUS-ADVANCE, third T if reading succeeded as in BUS-READ, NIL otherwise."
  (declare (type bus bus)
           (type (integer (0))
                 length))
  (multiple-value-bind (out success?)
      (bus-read bus length)
    (if success?
        (values out (bus-advance bus length)
                t)
        (values out (make-bus)
                nil))))

(defmacro with-bus (var stream
                    &body body)
  `(let ((,var (bus<- ,stream)))
     (multiple-value-prog1
         (progn
           ,@body)
       (setf (bus-stream ,var)
             nil))))

(defmacro with-file-bus ((var filespec
                              &rest options)
                         &body body)
  (let ((g!stream (gensym "stream")))
    `(with-open-file (,g!stream ,filespec ,@options)
       (with-bus ,var ,g!stream
         ,@body))))
